// energy.cpp

#ifndef ENERGY_CPP
#define ENERGY_CPP

#include <cstdlib>


int Energy(int current, int above, int below, int left, int right){
    int returnvalue = 0;
    if(above != -1)// r != 0
        returnvalue += std::abs(current - above);
    if(below != -1)// r != numrows-1
        returnvalue += std::abs(current - below);
    if(left != -1)// c != 0
        returnvalue += std::abs(current - left);
    if(right != -1)// c != numcols-1
        returnvalue += std::abs(current - right);
    return returnvalue;
}

int Min(int a, int b, int c){
    if(a<=b && a<=c)
        return a;
    if(b<=a && b<=c)
        return b;
    else
        return c;
}

#endif