// pic.hpp
// Michael Hallai mch81

#ifndef PIC_CPP
#define PIC_CPP

#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cmath>
#include <climits>
#include <algorithm>

class Pic {
   private:
      // .ppm files are in rgb color, and .pgm files are greyscale.
      bool PPM; 

      // Initail or current dimensions of the 
      // image as seams are added or removed.
      int current_width;
      int current_height;

      // Max value encoded in image header.
      int maxsize;

      // Dimensions for the final, seam carved, image.
      int end_width;
      int end_height;

   public:
      // Constructors:
      Pic(std::string, int, int, bool);
};

// Constructor:
Pic::Pic(std::string file, int v, int h, bool color){

   // Determine if in color.
   PPM = color;

   // Open input file
   std::fstream infile;
   infile.open(file.c_str());

   // If a file is not opened, then close the program
   if(!infile){
      std::cout << "\nFILE NOT OPENED\n";
      return;
   }

   // line to be read from file
   std::string inputLine;

   // Checks the version of the input file.
   getline(infile, inputLine);
   if(color){
      if (inputLine.compare("P3")!=0){
         std::cout << "Version error" << std::endl;
         return;
      }
   }
   else{
      if (inputLine.compare("P2")!=0){
         std::cout << "Version error" << std::endl;
         return;
      }
   }
   std::string version = inputLine;

   // Open output file
   std::ofstream outfile(((file.substr(0,file.length()-4))
      +"_processed"+file.substr(file.length()-4,4)).c_str());

   // Write the version to the file.
   outfile << version;

   do {
      getline(infile, inputLine);
      if(inputLine[0] == '#')
         outfile << std::endl << inputLine;
   } while (inputLine[0] == '#');

   std::stringstream ss(inputLine);

   // Finds the total current_width, current_height, and maxsize
   ss >> current_width >> current_height;
   end_width = current_width - v;
   end_height = current_height - h;

   // Set starting dimentions to be the greater of end and current value.
   int start_width, start_height;
   if(end_width > current_width)   start_width = end_width;
   else                            start_width = current_width;
   if(end_height > current_height) start_height = end_height;
   else                            start_height = current_height;

   infile >> maxsize;
   outfile <<"\n"<< end_width <<" "<< end_height <<"\n"<< maxsize <<"\n";

   // Declare array to store original image values.
   int original[start_height][start_width][3];

   // Loop to fill original array with values from file.
   for(int r=0; r<current_height && infile; r++){
      for(int c=0; c<current_width && infile; c++){
         if(color)
            infile >> original[r][c][0] >> original[r][c][1] 
                   >> original[r][c][2];
         else
            infile >> original[r][c][0];
      }
   }

   infile.close();

   // Declare and set initial flag values.
   bool done = false;
   bool decreasing_width = false;
   bool decreasing_height = false;
   bool increasing_width = false;
   bool increasing_height = false;
   if((current_width == end_width) && (current_height == end_height))
      done = true;
   if(end_width<1 || end_height<1)
      done = true;
   if((end_width/2>current_width) || (end_height/2>current_height))
      done = true;
   if(current_width > end_width)
      decreasing_width = true;
   if(current_height > end_height)
      decreasing_height = true;
   if(current_width < end_width)
      increasing_width = true;
   if(current_height < end_height)
      increasing_height = true;

   while(!done){

      // Declare array to store energy values
      int energy[current_height][current_width];

      // Loop to fill array
      // For Color:
      int E = 0;
      for(int r=0; r<current_height; r++){
         for(int c=0; c<current_width; c++){
            if(color){
               if(r != 0)
                  E += std::abs(original[r][c][0] - original[r-1][c][0])
                     + std::abs(original[r][c][1] - original[r-1][c][1])
                     + std::abs(original[r][c][2] - original[r-1][c][2]);
               if(c != 0)
                  E += std::abs(original[r][c][0] - original[r][c-1][0])
                     + std::abs(original[r][c][1] - original[r][c-1][1])
                     + std::abs(original[r][c][2] - original[r][c-1][2]);
               if(r != current_height-1)
                  E += std::abs(original[r][c][0] - original[r+1][c][0])
                     + std::abs(original[r][c][1] - original[r+1][c][1])
                     + std::abs(original[r][c][2] - original[r+1][c][2]);
               if(c != current_width-1)
                  E += std::abs(original[r][c][0] - original[r][c+1][0])
                     + std::abs(original[r][c][1] - original[r][c+1][1])
                     + std::abs(original[r][c][2] - original[r][c+1][2]);
            }
            else{
               if(r != 0)
                  E += std::abs(original[r][c][0] - original[r-1][c][0]);
               if(c != 0)
                  E += std::abs(original[r][c][0] - original[r][c-1][0]);
               if(r != current_height-1)
                  E += std::abs(original[r][c][0] - original[r+1][c][0]);
               if(c != current_width-1)
                  E += std::abs(original[r][c][0] - original[r][c+1][0]);
            }
            energy[r][c] = E;
            E = 0;
         }
      }
        
      // Array for storing cumulative energy seams
      int seams[current_height][current_width];

      // Loop to fill array
      // For vertical seams:
      if(decreasing_width || increasing_width)
         for(int r=0; r<current_height; r++){
            for(int c=0; c<current_width; c++){
               if(r==0)
                  seams[r][c] = energy[r][c];
               else if (c == 0)
                  seams[r][c] = energy[r][c] + 
                  std::min(seams[r-1][c], seams[r-1][c+1]);
               else if (c == current_width-1)
                  seams[r][c] = energy[r][c] + 
                  std::min(seams[r-1][c], seams[r-1][c-1]);
               else
                  seams[r][c] = energy[r][c] + std::min
                  (std::min(seams[r-1][c-1], seams[r-1][c]), seams[r-1][c+1]);
            }
         }
      // For horizontal seams:
      else if(decreasing_height)
         for(int c=0; c<current_width; c++){
            for(int r=0; r<current_height; r++){
               if(c==0)
                  seams[r][c] = energy[r][c];
               else if (r == 0)
                  seams[r][c] = energy[r][c] + 
                  std::min(seams[r][c-1], seams[r+1][c-1]);
               else if (r == current_height-1)
                  seams[r][c] = energy[r][c] + 
                  std::min(seams[r][c-1], seams[r-1][c-1]);
               else
                  seams[r][c] = energy[r][c] + std::min
                  (std::min(seams[r-1][c-1], seams[r][c-1]), seams[r+1][c-1]);
            }
         }



      if(increasing_width){
         int toadd = end_width-current_width;
         int addv[toadd];
         for(int i=0; i<toadd; i++){addv[i]=-1;}
         int least = INT_MAX;
         for(int seam=0; seam<toadd; seam++){
            for(int c=0; c<current_width; c++){
               if(seams[current_height-1][c] < least){
                  bool alreadyThere = false;
                  for(int i=0; i<toadd; i++){
                     if(c == addv[i])
                        alreadyThere = true;
                  }
                  if(!alreadyThere)
                     least = seams[current_height-1][c];
               }
            }
            int col = 0;
            while (seams[current_height-1][col] != least)
               col++;
            bool inserted = false;
            for(col = 0; !inserted; col++){
               if(seams[current_height-1][col] == least){
                  bool alreadyThere = false;
                     for(int i=0; i<toadd; i++){
                        if(col == addv[i])
                           alreadyThere = true;
                     }
                     if(!alreadyThere){
                        inserted = true;
                        addv[seam] = col;
                     }
               }
            }
            least = INT_MAX;
         }

         // for each seam to add
         for(int seam=0; seam<toadd; seam++){
            int mc = addv[seam];
            // for each row, starting at bottom
            for(int row=current_height-1; row>=0; row--){//std::cout << "check\n";
               // for each cell in row, starting at right edge.
                  if(color){
                     for(int col = current_width-1; col>=mc; col--){
                        original[row][col+1][0] = original[row][col][0];
                        original[row][col+1][1] = original[row][col][1];
                        original[row][col+1][2] = original[row][col][2];
                     }
                     if(mc==0){
                        original[row][mc][0] = original[row][mc+1][0];
                        original[row][mc][1] = original[row][mc+1][1];
                        original[row][mc][2] = original[row][mc+1][2];
                     }
                     else if(mc==current_width-1){
                        original[row][mc][0] = original[row][mc-1][0];
                        original[row][mc][1] = original[row][mc-1][1];
                        original[row][mc][2] = original[row][mc-1][2];
                     }
                     else{
                        original[row][mc][0] = (original[row][mc-1][0]+
                                                original[row][mc+1][0])/2;
                        original[row][mc][1] = (original[row][mc-1][1]+
                                                original[row][mc+1][1])/2;
                        original[row][mc][2] = (original[row][mc-1][2]+
                                                original[row][mc+1][2])/2;
                     }
                  }
                  else{
                     for(int col = current_width-1; col>=mc; col--)
                        original[row][col+1][0] = original[row][col][0];
                     if(mc==0)
                        original[row][mc][0] = original[row][mc+1][0];
                     else if(mc==current_width-1)
                        original[row][mc][0] = original[row][mc-1][0];
                     else
                        original[row][mc][0] = (original[row][mc-1][0]+original[row][mc+1][0])/2;
                  }

                  // update mc.
                  if(mc == 0){
                     if(seams[row-1][mc+1] < seams[row-1][mc])
                        mc+=1;
                  }
                  else if(mc == current_width-1){
                     if(seams[row-1][mc-1] <= seams[row-1][mc])
                        mc-=1;
                  }
                  else{
                     if((seams[row-1][mc-1] <= seams[row-1][mc]) && (seams[row-1][mc-1] <= seams[row-1][mc+1]))
                        mc-=1;
                     if((seams[row-1][mc+1] < seams[row-1][mc]) && (seams[row-1][mc+1] < seams[row-1][mc-1]))
                        mc+=1;
                  }
            }
            current_width++;
            for(int s=0; s<toadd; s++){
               if(addv[s] > addv[seam])
                  addv[s]++;
            }
         }
      }
      else if(increasing_height){
         int toadd = end_height-current_height;
         int addv[toadd];
         for(int i=0; i<toadd; i++){addv[i]=-1;}
         int least = INT_MAX;
         for(int seam=0; seam<toadd; seam++){
            for(int c=0; c<current_height; c++){
               if(seams[c][current_width-1] < least){
                  bool alreadyThere = false;
                  for(int i=0; i<toadd; i++){
                     if(c == addv[i])
                        alreadyThere = true;
                  }
                  if(!alreadyThere)
                     least = seams[c][current_width-1];
               }
            }
            int row = 0;
            while (seams[row][current_width-1] != least)
               row++;
            bool inserted = false;
            for(row = 0; !inserted; row++){
               if(seams[row][current_width-1] == least){
                  bool alreadyThere = false;
                     for(int i=0; i<toadd; i++){
                        if(row == addv[i])
                           alreadyThere = true;
                     }
                     if(!alreadyThere){
                        inserted = true;
                        addv[seam] = row;
                     }
               }
            }
            least = INT_MAX;
         }

         // for each seam to add
         for(int seam=0; seam<toadd; seam++){
            int mc = addv[seam];
            // for each row, starting at bottom
            for(int col=current_width-1; col>=0; col--){
               // for each cell in row, starting at right edge.
               if(color){
                  for(int row = current_height-1; row>=mc; row--){
                     original[row+1][col][0] = original[row][col][0];
                     original[row+1][col][1] = original[row][col][1];
                     original[row+1][col][2] = original[row][col][2];
                  }
                  if(mc==0){
                     original[mc][col][0] = original[mc+1][col][0];
                     original[mc][col][1] = original[mc+1][col][1];
                     original[mc][col][2] = original[mc+1][col][2];
                  }
                  else if(mc==current_height-1){
                     original[mc][col][0] = original[mc-1][col][0];
                     original[mc][col][1] = original[mc-1][col][1];
                     original[mc][col][2] = original[mc-1][col][2];
                  }
                  else{
                     original[mc][col][0] = (original[mc-1][col][0]+
                                             original[mc+1][col][0])/2;
                     original[mc][col][1] = (original[mc-1][col][1]+
                                             original[mc+1][col][1])/2;
                     original[mc][col][2] = (original[mc-1][col][2]+
                                             original[mc+1][col][2])/2;
                  }
               }
               else{
                  for(int row = current_height-1; row>=mc; row--)
                     original[row+1][col][0] = original[row][col][0];
                  if(mc==0)
                     original[mc][col][0] = original[mc+1][col][0];
                  else if(mc==current_height-1)
                     original[mc][col][0] = original[mc-1][col][0];
                  else
                     original[mc][col][0] = (original[mc-1][col][0]+
                                             original[mc+1][col][0])/2;
               }

               // update mc.
               if(mc == 0){
                  if(seams[mc+1][col-1] < seams[mc][col-1])
                     mc+=1;
               }
               else if(mc == current_height-1){
                  if(seams[mc-1][col-1] <= seams[mc][col-1])
                     mc-=1;
               }
               else{
                  if((seams[mc-1][col-1] <= seams[mc][col-1]) && (seams[mc-1][col-1] <= seams[mc+1][col-1]))
                     mc-=1;
                  if((seams[mc+1][col-1] < seams[mc][col-1]) && (seams[mc+1][col-1] < seams[mc-1][col-1]))
                     mc+=1;
               }
            }
            current_height++;
            for(int s=0; s<toadd; s++){
               if(addv[s] > addv[seam])
                  addv[s]++;
            }
         }
      }

      // Find seam starting point with least cumulative energy.
      int least = INT_MAX;
      // For vertical seams:
      if(decreasing_width){
         for(int c=0; c<current_width; c++)
            if(seams[current_height-1][c] < least)
               least = seams[current_height-1][c];
      }
      // For horizontal seams:
      else if(decreasing_height){
         for(int r=0; r<current_height; r++){
            if(seams[r][current_width-1] < least)
               least = seams[r][current_width-1];
         }
      }
      // set col to the first instance of least.
      int lowest;
      int col = 0;
      int row = 0;
      // For vertical seams:
      if(decreasing_width)
         while (seams[current_height-1][col] != least){col++;}
      // For horizontal seams:
      else if(decreasing_height)
         while (seams[row][current_width-1] != least){row++;}

      // shift all pixels over to remove seam. 
      // For vertical seams:
      if(decreasing_width){
         for(int i=current_height-1; i>=0; i--){
            for(int mc = col; mc<current_width; mc++){
               original[i][mc][0] = original[i][mc+1][0];
               original[i][mc][1] = original[i][mc+1][1];
               original[i][mc][2] = original[i][mc+1][2];
            }
            if(col == 0){
               if(seams[i-1][col+1] < seams[i-1][col])
                  col+=1;
            }
            else if(col == current_width-1){
               if(seams[i-1][col-1] <= seams[i-1][col])
                  col-=1;
            }
            else{
               if((seams[i-1][col-1] <= seams[i-1][col]) && (seams[i-1][col-1] <= seams[i-1][col+1]))
                  col-=1;
               if((seams[i-1][col+1] < seams[i-1][col]) && (seams[i-1][col+1] < seams[i-1][col-1]))
                  col+=1;
            }
         }
      }
      // For horizontal seams:
      else if(decreasing_height){
         for(int i=current_width-1; i>=0; i--){
            for(int mc = row; mc<current_height; mc++){
               original[mc][i][0] = original[mc+1][i][0];
               original[mc][i][1] = original[mc+1][i][1];
               original[mc][i][2] = original[mc+1][i][2];
            }
            if(row == 0){
               if(seams[row+1][i-1] < seams[row][i-1])
                  row+=1;
            }
            else if(row == current_width-1){
               if(seams[row-1][i-1] <= seams[row][i-1]);
                  row-=1;
            }
            else{
               if((seams[row-1][i-1] <= seams[row][i-1]) && (seams[row-1][i-1] <= seams[row+1][i-1]))
                  row-=1;
               if((seams[row+1][i-1] < seams[row][i-1]) && (seams[row+1][i-1] < seams[row-1][i-1]))
                  row+=1;
            }
         }
      }

      // Change flag values.
      if(decreasing_width)
         current_width--;
      else if(decreasing_height)
         current_height--;
      if(increasing_width){
         current_width = end_width;
         increasing_width = false;
      }
      else if(increasing_height){
         current_height = end_height;
         increasing_height = false;
      }
      if(decreasing_width && current_width==end_width)
         decreasing_width = false;
      if(decreasing_height && current_height==end_height)
         decreasing_height = false;
      if(!decreasing_height && !decreasing_width
      && !increasing_height && !increasing_width)
         done=true;
   }// end for loop///////////////////////////////////////////////////////////

   // Print image of new size to the file. 
   for(int r=0; r<end_height; r++){
      for(int c=0; c<end_width; c++){
         if(color)
            outfile << original[r][c][0] << " " << original[r][c][1]
                    << " " << original[r][c][2] << " ";
         else
            outfile << original[r][c][0] << " ";
      }
      outfile << "\n";
   }

   // Close the file
   outfile.close();
}

#endif