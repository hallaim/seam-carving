//---------------------------------------------------------------------------//
// main.cpp
// Michael HAllai mch81
// Dec 8th 2015
// Algorithms Assignment 4: Seam Carving Algorithm
//---------------------------------------------------------------------------//

#include "pic.h"

#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cmath>
#include <climits>

// a.exe image.pgm v h     Number of (v)ertical and (h)orizontal seams to remove
// Negative v and h values will add seams to the image, increasing it's size
int main(int argc, char *argv[]) {
   if (argc == 4) {
      std::string fname = argv[1];
      int v = atoi(argv[2]);
      int h = atoi(argv[3]);
      bool color = (fname.substr(fname.length()-4,4) == ".ppm");
      Pic(fname, v, h, color);
   }
   // Else: If the number of inputs given is not correct, close the program.  
   else
      std::cout << "Invalid number of inputs\n";

   return 0;
}