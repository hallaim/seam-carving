Programmer: Michael Hallai mch81
Class: Algorithms
Project: 4 - Seam Carving
Date: TUE 12/08/15

Language: C++

USE:
    Program.exe image.pgm v h   // image - input image
                image.ppm       // v - # of vertical rows to remove or add
                                // h - # of horizontal rows
                                            
        // The program will generate an output file "image_processed.pgm"
        // 'v' and 'h' can be positive or negative.  
        // Positive values will remove seams from the file.
        // Negative values will add seams to the file.

        // The input image can be two formats:
        //      - .pgm (greyscale)
        //      - .ppm (rgb ascii encoded color image)